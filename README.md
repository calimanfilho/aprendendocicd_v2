## Aplicação Web em um Contêiner Apache HTTPD
Neste projeto, que faz parte de uma série de aulas da [DIO](https://www.dio.me/), será criado um pipeline de deploy de uma aplicação com cenários de produção de imagens com Docker e criação de deployments em um cluster kubernetes em nuvem utilizando o GCP.


Estrutura do projeto:
```
.
├── app
|   ├── conexao.php
|   ├── css.css
|   ├── dockerfile
|   ├── incluir.php
|   ├── index.html
|   └── js.js
├── images
|   └── arquitetura-do-cluster-e-aplicacao.png
├── mysql
|   ├── dockerfile
|   └── sql.sql
├── app-deployment.yaml
├── gitlab-ci.yaml
├── load-balancer.yaml
├── mysql-deployment.yaml
├── README.md
├── script.sh
└── secrets.yaml

```

## Arquitetura do Cluster e Aplicação

<img src = "images/arquitetura-do-cluster-e-aplicacao.png">